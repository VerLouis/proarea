<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class WeezEventAPIController extends AbstractController
{
    private $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    function getApiKey(){
        return $this->getParameter('app.weezevent_api_key');
    }

    private function getAccessToken(): string
    {
        $login = $this->getParameter('app.weezevent_login');
        $pw = $this->getParameter('app.weezevent_pw');
        try {
            $response = $this->client->request(
            'POST',
            'https://api.weezevent.com/auth/access_token', [
                'headers' => [
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ],
                'body' => '&username=' . $login . '&password=' . $pw . '&api_key='.$this->getApiKey().''
            ]);
        } catch (\Throwable $th) {
            return "error:" . $th;
        }
        $content = $response->toArray();
        return $content["accessToken"];
    }

    private function getWeezEventData(string $want){
        try {
            $response = $this->client->request(
            'GET',
            'https://api.weezevent.com/'.$want.'?&api_key='.$this->getApiKey().'&access_token='. $this->getAccessToken() .'&include_without_sales=true', [
                'headers' => [
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ],
            ]);
        } catch (\Throwable $th) {
            return "error:" . $th;
        }
        $content = $response->toArray();
        return $content;
    }
    /**
     * @Route("/weezevent/events", name="app_weezevent_users")
     */
    public function events(): Response
    {
        
        $event = $this->getWeezEventData('event/categories');
        return new JsonResponse($event);
    }
}
